const {Point, Vector, Circle, Line, Ray, Segment, Arc, Box, Polygon, Matrix, PlanarSet, BooleanOperations} = require('@flatten-js/core');

/*
Texture: represents an image texture, with a variable to show when it has loaded
@string src: the relative source for the image
*/
function Texture(src) {
	this.loaded = false;
  createImage(src).then((img) => {
		this.image = img;
		this.loaded = true;
	});
}

/*
creates and loads a new texture, adding it to gameArea.textures
@string name: the unique identifier key associated with the texture
@string src: the path to the source location for the image
@Palatte initialPalatte: the palatte (if any) to be applied to the image, on load
*/
function createTexture(name, src, initialPalatte = null) {
  let texture = new Texture(src);
  gameArea.textures[name] = texture;
}

/*
Color: represents a basic set of rgba color data
@number r: the red value of the color
@number g: the green value of the color
@number b: the blue value of the color
@number a: the alpha (opacity) value of the color
*/
class Color {
  constructor(r, g, b, a = 255) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }

  get cssStyle() {
    return `rgb(${this.r} ${this.g} ${this.b} / ${this.a / 255})`
  }
}

/*
describes a collection of colors, to be applied to an image
@Color[] colors: the colors to be made into a palatte
*/
function Palatte(...colors) {
  this.colors = colors;
}

var editorPalatte = new Palatte(
  new Color(0, 0, 0, 255),

  new Color(255, 0, 0, 255),
  new Color(0, 255, 0, 255),
  new Color(0, 0, 255, 255),

  new Color(255, 255, 0, 255),
  new Color(0, 255, 255, 255),
  new Color(255, 0, 255, 255),

  new Color(255, 127, 0, 255),
  new Color(0, 255, 127, 255),
  new Color(127, 0, 255, 255),

  new Color(127, 255, 0, 255),
  new Color(0, 127, 255, 255),
  new Color(255, 0, 127, 255),

  new Color(255, 255, 255, 255)
);

/*
creates and loads an Image object
@string src: the path to the source location for the image
*/
function createImage(src) {
	const promise = new Promise((resolve, reject) => {
	  let img = document.createElement("img");
		img.onerror = (err) => {
			console.error('Texture failed to load', err);
			reject(err);
		};
		img.onload = () => {
			console.log('Texture loaded!', img.naturalWidth);
			resolve(img);
		}
	  img.src = src;
	});
  return promise;
}

/*
converts an Image element to a a new ImageData object representing it
@Image img: the element to convert
*/
function imageToImageData(img) {
  let canv = document.createElement('canvas');
  let ctx = canv.getContext('2d');

  canv.width = img.naturalWidth;
  canv.height = img.naturalHeight;
  ctx.imageSmoothingEnabled = false;
  ctx.drawImage(img, 0, 0, img.naturalWidth, img.naturalHeight);

  let ctximgdata = ctx.getImageData(0, 0, img.naturalWidth, img.naturalHeight);

  let imgdata = new ImageData(
    new Uint8ClampedArray(ctximgdata.data),
    ctximgdata.width,
    ctximgdata.height);

  return imgdata;
}

/*
converts an ImageData object to a new Image element, containing the same data
@ImageData imgdata: the object to convert
*/
function imageFromImageData(imgdata) {
  let canv = document.createElement('canvas');
  let ctx = canv.getContext('2d');

  canv.width = imgdata.width;
  canv.height = imgdata.height;
  ctx.imageSmoothingEnabled = false;
  canv.getContext('2d').putImageData(imgdata, 0, 0);

  let newimg = createImage(canv.toDataURL());

  return newimg;
}

/*
converts an ImageData object to a rectangular jagged array of Color objects, representing the matrix of pixels locationally
@ImageData imgdata: the object to convert
*/
function imageDataTo2dPixelArray(imgdata) {
  let pixArray = new2dArray(imgdata.width, imgdata.height);

  for (let i = 0; i < imgdata.data.length; i += 4) {
    let currentX = (i / 4) % (imgdata.width);
    let currentY = Math.floor((i / 4) / (imgdata.width));

    let currentPixel = new Color(imgdata.data[i], imgdata.data[i+1], imgdata.data[i+2], imgdata.data[i+3]);

    pixArray[currentX][currentY] = currentPixel;
  }

  return pixArray;
}

/*
converts a rectangular jagged array of Color objects to an ImageData object
@Array pixArray: the array to convert
*/
function imageDataFrom2dPixelArray(pixArray) {
  let array = new Uint8ClampedArray(4 * pixArray.length * pixArray[0].length);

  for (let i = 0; i < pixArray.length; i++) {
    for (let j = 0; j < pixArray[i].length; j++) {
      array[(j * pixArray.length + i) * 4]      = pixArray[i][j].r;
      array[(j * pixArray.length + i) * 4 + 1]  = pixArray[i][j].g;
      array[(j * pixArray.length + i) * 4 + 2]  = pixArray[i][j].b;
      array[(j * pixArray.length + i) * 4 + 3]  = pixArray[i][j].a;
    }
  }

  return new ImageData(array, pixArray.length, pixArray[0].length);
}

/*
applies a specific function to the provided image, allowing direct pixel manipulation
@Image img: the image to be modified
@function func: a function which takes an ImageData object as its parameter
*/
function modifyImage(img, func) {
  let imgdata = imageToImageData(img);

  imgdata = func(imgdata);

  return imageFromImageData(imgdata);
}

/*
applies a specific function to the provided image, allowing direct pixel manipulation.
the function is provided with a 2-dimensional array of Colors representing the pixels, making the data much easier to work with.
this, however, may increase processing time.
@Image img: the image to be modified
@function func: a function which takes an ImageData object as its parameter
*/
function easyModifyImage(img, func) {
  let imgData = imageToImageData(img);

  let pixArray = imageDataTo2dPixelArray(imgData);

  pixArray = func(pixArray);

  imgData = imageDataFrom2dPixelArray(pixArray);

  return imageFromImageData(imgData);
}

/*
creates an outline in the specified color around the opaque portion of a semitransparent image
@Image img: the image to be modified
@Color color: the color of the outline
*/
function outlineImage(img, color) {
  return easyModifyImage(img, function(pixArray) {
    let newPixArray = JSON.parse(JSON.stringify(pixArray));
    for (let i = 0; i < pixArray.length; i++) {
      for (let j = 0; j < pixArray[0].length; j++) {
        let currentPixel = pixArray[i][j];
        let leftPixel = i>0 ? pixArray[i-1][j] : new Color(0,0,0,0);
        let rightPixel = i<pixArray.length-1 ? pixArray[i+1][j] : new Color(0,0,0,0);
        let upPixel = j>0 ? pixArray[i][j-1] : new Color(0,0,0,0);
        let downPixel = j<pixArray[0].length-1 ? pixArray[i][j+1] : new Color(0,0,0,0);

        if (currentPixel.a !== 255 && (
          leftPixel.a === 255 ||
          rightPixel.a === 255 ||
          upPixel.a === 255 ||
          downPixel.a === 255
        )) {
          newPixArray[i][j] = color;
        }
        else {
          newPixArray[i][j] = pixArray[i][j];
        }
      }
    }

    return newPixArray;
  });
}

/*
applies a specific palatte to a prepared image
@Image img: the image to be modified
@Palatte palatte: the palatte to be applied
*/
function applyPalatte(img, palatte) {
  return modifyImage(img, function(imgdata) {

      for (let i = 0; i < imgdata.data.length; i += 4) {
        let color = new Color(imgdata.data[i], imgdata.data[i+1], imgdata.data[i+2], imgdata.data[i+3]);

        if (palatte.colors.length-1 >= color.r && allEqual(color.r, color.g, color.b)) {
          let newColor = palatte.colors[color.r];
          imgdata.data[i]   = newColor.r;
          imgdata.data[i+1] = newColor.g;
          imgdata.data[i+2] = newColor.b;
        }
        else {
          console.log("image does not match the supplied palatte", img, palatte);
          return null;
        }
      }

      return imgdata;
  });
}

/*
reverse-applies a specific palatte to an image, turning it into an image ready for processing via applyPalatte
@Image img: the image to be modified
@Palatte palatte: the palatte to be removed
*/
function reversePalatte(img, palatte) {
  return modifyImage(img, function(imgdata) {
      for (let i = 0; i < imgdata.data.length; i += 4) {
        let color = new Color(imgdata.data[i], imgdata.data[i+1], imgdata.data[i+2], imgdata.data[i+3]);

        let colorChanged = false;
        palatte.colors.forEach(function (val, index) {
          if (color.r == val.r && color.g == val.g && color.b == val.b) {
            imgdata.data[i] = index;
            imgdata.data[i+1] = index;
            imgdata.data[i+2] = index;

            colorChanged = true;
            return;
          }
        });

        if (!colorChanged) {
          console.log("image does not match the supplied palatte", img, palatte);
          return null;
        }
      }
      return imgdata;
  });
}

/*
converts a geometric basic shape to a drawable Path2D
@Point, @Vector, @Line, @Ray, @Segment, @Arc, @Circle, @Box shape: the shape to be converted
*/
function shapeToPath(shape) {
  return polygonToPath(new Polygon(shape));
}

/*
converts a geometric polygon to a drawable Path2D
@Polygon polygon: the polygon to be converted
*/
function polygonToPath(polygon) {
  if (/ d="([^"]+)"/g.exec(polygon.svg()) != null) {
    return new Path2D(/ d="([^"]+)"/g.exec(
      polygon.svg()
    )[1]
      .replaceAll("\n", " ")
      .replaceAll(/ +/g, " ")
      .replace(/^ /g, ""));
  }
  else {
    return new Path2D(polygon.svg());
  }
}