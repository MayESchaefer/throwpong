createTexture("paddle", "res/sprite/anim/paddle.png");

function init() {
  document.title = "Throwpong (working title)";

  var moved = false;

  const particleSystem = new ParticleSystem();
  gameArea.components.push(
    new Component(
      "particleSystem", 
      {}, 
      particleSystem.process.bind(particleSystem), 
      particleSystem.draw.bind(particleSystem)
    )
  );

  gameArea.components.push(
    new Component("player",
			{ width: 30, height: 30, color: "black", x: 1280 / 2 - 30 / 2, y: 720 - 40, lastx: 0, lasty: 0, speed: 10 },
      function() {
				this.params.lastx = this.params.x;
				this.params.lasty = this.params.y;
				
        if (gameArea.key(37) || gameArea.key(charCode("A")))
          {this.params.x += 0 - this.params.speed; moved = true; }
        if (gameArea.key(39) || gameArea.key(charCode("D")))
          {this.params.x += this.params.speed; moved = true ;}
      },
      function(time, delta, tickDelta, ift) {
        ctx = gameArea.context;
        ctx.fillStyle = this.params.color;
        ctx.fillRect(this.params.x * ift + this.params.lastx * (1-ift), this.params.y * ift + this.params.lasty * (1-ift), this.params.width, this.params.height);
      }
    )
  );
	
  gameArea.components.push(
    new Component("paddle",
      { width: 30, height: 30, color: "black", x: 0, y: 0, lastx: 0, lasty: 0, speed: 20, state: "inactive", waitTimer: 0 },
      function() {
				this.params.lastx = this.params.x;
				this.params.lasty = this.params.y;
				
				if (this.params.state === "shooting") {
	        let ballParams = componentFromName("ball").params;
					this.params.y -= this.params.speed;
					if (ballParams.x >= this.params.x && ballParams.x <= this.params.x + this.params.width &&
							ballParams.y + ballParams.radius <= this.params.y + this.params.height && ballParams.y - ballParams.radius >= this.params.y) {
						this.params.color = "red";
						this.params.state = "pausing";
					}
					if (this.params.y < 720 - 340) {
						this.params.state = "pausing";
					}
				}
				else if (this.params.state === "pausing") {
					this.params.waitTimer++;
					if (this.params.waitTimer > 5) {
						this.params.waitTimer = 0;
						this.params.state = "returning";
					}
				}
				else if (this.params.state === "returning") {
	        let playerParams = componentFromName("player").params;
					let playerMidpointX = playerParams.x + playerParams.width / 2;
					let playerMidpointY = playerParams.y + playerParams.height / 2;
					
					this.params.y += this.params.speed;
					
					if (playerMidpointX >= this.params.x && playerMidpointX <= this.params.x + this.params.width &&
							playerMidpointY <= this.params.y + this.params.height && playerMidpointY >= this.params.y) {
						this.params.state = "inactive";
					}
					let frameParams = componentFromName("frame").params;
					if (this.params.y > frameParams.height + frameParams.y) {
						this.params.state = "inactive";
						componentFromName("score").params.paddles--;
					}
				}
        else if (gameArea.key(32) || gameArea.key(charCode("W"))) {
	        let playerParams = componentFromName("player").params;
					this.params.x = playerParams.x;
					this.params.y = playerParams.y;
					this.params.lastx = this.params.x;
					this.params.lasty = this.params.y;
					this.params.state = "shooting";
					this.params.color = "black";
				}
      },
      function(time, delta, tickDelta, ift) {
				if (this.params.state === "inactive") {
					return;
				}
        ctx = gameArea.context;
        ctx.fillStyle = this.params.color;
        ctx.fillRect(this.params.x * ift + this.params.lastx * (1-ift), this.params.y * ift + this.params.lasty * (1-ift), this.params.width, this.params.height);
      }
    )
  );

  gameArea.components.push(
    new Component("ball",
      { radius: 10, color: "black", x: 1280 / 2, y: 720 - 60, lastx: 0, lasty: 0, speedx: 7, speedy: 3 },
      function() {
				this.params.lastx = this.params.x;
				this.params.lasty = this.params.y;
				
        if (!moved) {
          return;
        }

        this.params.x += this.params.speedx;
        this.params.y += this.params.speedy;

        let frameParams = componentFromName("frame").params;
        if (this.params.x + this.params.radius >= frameParams.width + frameParams.x ||
          this.params.x - this.params.radius >= frameParams.width + frameParams.x ||
          this.params.x - this.params.radius <= frameParams.x ||
          this.params.x + this.params.radius <= frameParams.x) {
          this.params.speedx = 0 - this.params.speedx;
          this.params.x += this.params.speedx * 2;
        }

        if (this.params.y + this.params.radius >= frameParams.height + frameParams.y ||
          this.params.y - this.params.radius >= frameParams.height + frameParams.y ||
          this.params.y - this.params.radius <= frameParams.y ||
          this.params.y + this.params.radius <= frameParams.y) {
          this.params.speedy = 0 - this.params.speedy;
          this.params.y += this.params.speedy * 2;
        }
      },
      function(time, delta, tickDelta, ift) {
        ctx = gameArea.context;
        ctx.beginPath();
        ctx.fillStyle = this.params.color;
        ctx.ellipse(this.params.x * ift + this.params.lastx * (1-ift), this.params.y * ift + this.params.lasty * (1-ift), this.params.radius, this.params.radius, 1, 0, 2 * Math.PI);
        ctx.fill();
      }
    )
  );

  let nextFlameTime = null;

  gameArea.components.push(
    new Component("frame",
      { width: 500, height: 700, color: "black", x: 1280 / 2 - 500 / 2, y: 720 / 2 - 700 / 2 }, null,
      function() {
        ctx = gameArea.context;
        ctx.beginPath();
        ctx.fillStyle = this.params.color;
        ctx.rect(this.params.x, this.params.y, this.params.width, this.params.height);
        ctx.stroke();
      }
    )
  );
	
	gameArea.components.push(
		new Component("score", { paddles: 5 }, null,
			function() {
				ctx = gameArea.context;
				ctx.fillStyle = "black";
				ctx.font = "20px sans-serif";
				ctx.fillText("paddles: " + this.params.paddles, 10, 20);
			}
		)
	);
	
  /*gameArea.components.push(
    new Component("delta", { }, null,
      function(time, delta, tickDelta, interpolationFactor) {
        ctx = gameArea.context;
        ctx.fillStyle = "black";
        ctx.font = "20px sans-serif";
        ctx.fillText(delta, 10, 20);
        ctx.fillText(tickDelta, 10, 40);
        ctx.fillText(interpolationFactor, 10, 60);
				//console.log(tickDelta, interpolationFactor);
      }
    )
  );*/

  gameArea.start();
}
