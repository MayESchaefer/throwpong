/*
contains most of the central game logic and information for running the game. should not need to be modified for basically any 2d game
@bool debug: whether or not the application was run with a debug flag
@element canvas: the html element for the drawing canvas
@object keyboard: contains info for the keyboard within the game window
  @set keys: the set of keys currently being pressed
  @set frameKeys: the set of keys pressed since the last update frame
@object cursor: contains info for the cursor within the game window
  @number x: the current horizontal location of the cursor on the game screen
  @number y: the current vertical location of the cursor on the game screen
  @bool click: whether or not the cursor is currently pressed
  @array clicks: the list of unprocessed clicks in the game
    @object
      @object start: the start position of the click
        @number x: the horizontal location of the point
        @number y: the vertical location of the point
      @array drag
        @object: a mid position of the click
          @number x: the horizontal location of the point
          @number y: the vertical location of the point
      @object end: the end position of the click
        @number x: the horizontal location of the point
        @number y: the vertical location of the point
      @number type: the button type number, with 0 being left, 1 being middle, and 2 being right
  @set clickStarts
    @object[type]: the key is the button type number
      @number x: the horizontal location of the point
      @number y: the vertical location of the point
@object touch: contains info for the touch controls of the game
  @number x: the last horizontal location of a touch on the game screen
  @number y: the last vertical location of a touch on the game screen
  @bool click: whether or not the user is currently touching the screen
  @array clicks: the list of unprocessed gestures in the game
    @object
      @object start: the start position of the gesture
        @number x: the horizontal location of the point
        @number y: the vertical location of the point
      @array drag
        @object: a mid position of the gesture
          @number x: the horizontal location of the point
          @number y: the vertical location of the point
      @object end: the end position of the gesture
        @number x: the horizontal location of the point
        @number y: the vertical location of the point
  @set clickStarts
    @object[type]: the key is the touch index
      @number x: the horizontal location of the point
      @number y: the vertical location of the point
@array components: the list of Components for the gameArea to run
  @Component
@bool stopped: a flag to tell the draw function to stop running
@DOMHighResTimeStamp lastGameTick: the timestamp for the last time the game logic tick updated
@array textures: an array of all Texture nodes
@object startupValues: contains values to be set in start()
	@int canvasWidth: the width the canvas should be set to
	@int canvasHeight: the height the canvas should be set to
	@int tickSpeed: the frequency at which processUpdate should be called (in milliseconds)
@function start(): initializes the gameArea and the game
@function key(@number keyCode): returns a boolean representing whether or not the specified keycode is pressed
@function clear(): clears the game canvas
@function stop(): stops the update and draw processes for the game
*/
var gameArea = {
  debug: (window.location.hash.substring(1) == "debug"),
  canvas: document.createElement("canvas"),
  keyboard: {
    keys: new Set(),
    frameKeys: new Set(),
  },
  cursor: {
    x: 0,
    y: 0,
    click: false,
    clicks: [],
    clickStarts: new Set(),
    clickDrags: new Set()
  },
  touch: {
    x: 0,
    y: 0,
    click: false,
    clicks: [],
    clickStarts: new Set(),
    clickDrags: new Set()
  },

  components: [],
  stopped: false,

  lastGameTick: performance.now(),

  textures: [],
	
	startupValues: {
		canvasWidth: 1280,
		canvasHeight: 720,
		tickSpeed: 50
	},

  start: function() {
    this.canvas.width = this.startupValues.canvasWidth;
    this.canvas.height = this.startupValues.canvasHeight;

    this.context = this.canvas.getContext("2d");
    this.context.imageSmoothingEnabled = false;//disable antialiasing

    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    this.updateInterval = setInterval(processUpdate, this.startupValues.tickSpeed);
    window.requestAnimationFrame(processDraw);

    //keyboard
    window.addEventListener('keydown', function (e) {
      if (!gameArea.key(e.keyCode)) {
        gameArea.keyboard.keys.add(e.keyCode);
        gameArea.keyboard.frameKeys.add(e.keyCode);
      }
    });
    window.addEventListener('keyup', function (e) {
      gameArea.keyboard.keys.delete(e.keyCode);
    });

    //mouse
    window.addEventListener('mousemove', function (e) {
      gameArea.cursor.x = e.pageX;
      gameArea.cursor.y = e.pageY;

      gameArea.cursor.clickStarts.forEachSet(function(value, key) {
        if (!gameArea.cursor.clickDrags[key]) { gameArea.cursor.clickDrags[key] = []; }

        gameArea.cursor.clickDrags[key].push({
          x: gameArea.cursor.x,
          y: gameArea.cursor.y
        });
      }, 5);
    });
    window.addEventListener('mousedown', function (e) {
      gameArea.cursor.click = true;

      gameArea.cursor.clickStarts[e.button] = {
        x: gameArea.cursor.x,
        y: gameArea.cursor.y
      };
    });
    window.addEventListener('mouseup', function (e) {
      gameArea.cursor.click = false;

      gameArea.cursor.clicks.push({
        start: gameArea.cursor.clickStarts[e.button],
        drag: gameArea.cursor.clickDrags[e.button],
        end: {
          x: gameArea.cursor.x,
          y: gameArea.cursor.y
        },
        type: e.button
      });
      delete gameArea.cursor.clickDrags[e.button];
      delete gameArea.cursor.clickStarts[e.button];
    });

    //touchscreen
    window.addEventListener('touchmove', function (e) {
      gameArea.touch.x = e.touches[0].clientX;
      gameArea.touch.y = e.touches[0].clientY;

      gameArea.touch.clickStarts.forEachSet(function(value, key) {
        if (!gameArea.touch.clickDrags[key]) { gameArea.touch.clickDrags[key] = []; }

        gameArea.touch.clickDrags[key].push({
          x: e.touches[key].clientX,
          y: e.touches[key].clientY
        });
      }, 5);
    });
    window.addEventListener('touchstart', function (e) {
      gameArea.touch.click = true;

      gameArea.touch.clickStarts[e.touches.length - 1] = {
        x: gameArea.touch.x,
        y: gameArea.touch.y
      };
    });
    var touchEnd = function (e) {
      gameArea.touch.click = (e.touches.length > 0);

      gameArea.touch.clicks.push({
        start: gameArea.touch.clickStarts[e.touches.length - 1],
        drag: gameArea.touch.clickDrags[e.touches.length - 1],
        end: {
          x: e.touches[e.touches.length - 1].clientX,
          y: e.touches[e.touches.length - 1].clientY
        }
      });
      delete gameArea.touch.clickDrags[e.touches.length - 1];
      delete gameArea.touch.clickStarts[e.touches.length - 1];
    }
    window.addEventListener('touchend', touchEnd);
    window.addEventListener('touchcancel', touchEnd);
  },
  key: function(keyCode) {
    return gameArea.keyboard.keys.has(keyCode);
  },
  clear: function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  },
  stop: function() {
    clearInterval(this.updateInterval);
    this.stopped = true;
  }
}

/*
Component: represents a callable component within the gameArea
@string name: used to identify the Component. not unique
@object params: values passed to and from the process and draw functions
@function process(params): runs every 50ms, used to process game logic
@function draw(time, delta, tickDelta, interpolationFactor): runs every 13ms, used to process animation and visuals
@function remove(): removes the Component from the list of executed Components
*/
function Component(name, params, process = null, draw = null) {
  this.name = name;
  this.params = params;
  this.process = process;
  this.draw = draw;

  this.remove = function() {
    gameArea.components = gameArea.components.filter(function(comp) {
      return comp !== this;
    }, this);
  }
}

/*
returns the first component with the given name
@string name: the name to search for
*/
function componentFromName(name) {
  return gameArea.components.find(comp => { return comp.name === name; });
}

/*
handles game logic within components
*/
function processUpdate() {
  const texturesLoaded = gameArea.textures.every(texture => texture.loaded);
  if (!texturesLoaded) {
    return;
  }
  gameArea.lastGameTick = performance.now();

  gameArea.components.forEach(function(componentFunction) {
    if (componentFunction.process != null) {
      componentFunction.process();
    };
  });

  gameArea.keyboard.frameKeys.clear();
  gameArea.cursor.clicks = [];
}

var lastDrawTime = performance.now();
/*
handles animation and visuals within components
@DOMHighResTimeStamp startTime: the exact call time of the process
*/
function processDraw(startTime) {
  const texturesLoaded = gameArea.textures.every(texture => texture.loaded);
  if (!texturesLoaded) {
    window.requestAnimationFrame(processDraw);
    return;
  }

  let delta = startTime - lastDrawTime;
  lastDrawTime = startTime;
	
	let tickDelta = startTime - gameArea.lastGameTick;
	let interpolationFactor = tickDelta / gameArea.startupValues.tickSpeed;

  gameArea.clear();
  gameArea.context.beginPath();

  gameArea.components.forEach(function(componentFunction) {
    if (componentFunction.draw != null) {
      componentFunction.draw(startTime, delta, tickDelta, interpolationFactor);
    };
  });
	
  if (!gameArea.stopped) {
  	window.requestAnimationFrame(processDraw);
  }
}
