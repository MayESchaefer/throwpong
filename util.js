const fs = require('fs');
const path = require('path');

/*
converts the first character of a string into its character code
@string string: the string of which the first character will be converted
*/
function charCode(string) {
  return string.charCodeAt(0);
}

/*
removes the provided element from the document
@node elem: the element to be removed
*/
function deleteElement(elem) {
  elem.parentNode.removeChild(elem);
}

/*
calls a function for every element in the set. only works for numeric keys
@function(value, index (optional), set (optional)) func: the function to call
  value: the current value of the item in the set
  @int index: the index of the item in the set
  @set set: the set
@number expectedMaxKey: the maximum key to search
@number expectedMinKey: the minimum key to search. defaults to 0
*/
Set.prototype.forEachSet = function(func, expectedMaxKey, expectedMinKey = 0, value = null) {
  for (let i = expectedMinKey; i < expectedMaxKey; i++) {
    if (this[i]) {
      func(this[i], i, this, value);
    }
  }
}

/*
returns true if all parameters supplied contain exactly the same value
@object[] vars: the objects to be compared
*/
function allEqual(...vars) {
  vars.forEach(function(val, index) {
    if (index > 0 && val !== vars[index-1]) {
      return false;
    }
  });
  return true;
}

/*
creates a deep clone of the provided object, providing that object is serializable
@object object: the object to be cloned
*/
function deepClone(object) {
	return JSON.parse(JSON.stringify(object));
}

/*
creates a rectangular jagged array with the specified number of rows and columns
@number width: length of external array
@number height: length of internal arrays
*/
function new2dArray(width, height) {
  let arr = new Array(width);
  for(let i = 0; i < arr.length; i++){
    arr[i] = new Array(height);
  }
  return arr;
}

/*
loads a json file from the disk and converts it from the json format to a javascript object
@string filePath: the relative or absolute path of the file to be read
*/
function loadJson(filePath) {
  let rawdata = fs.readFileSync(path.resolve(__dirname, filePath));
  return JSON.parse(rawdata);
}

/*
saves a javascript object to the disk in json format
obj: the object to be saved to the disk
@string filePath: the relative or absolute path of the file to be written
*/
function saveJson(obj, filePath) {
  fs.writeFileSync(path.resolve(__dirname, filePath), JSON.stringify(obj));
}

/*
converts radians to degrees
deg: degrees
@number: radians
*/
function degToRad(deg) {
  return deg * Math.PI / 180
}

/*
converts degrees to radians
rad: radians
@number: degrees
*/
function radToDeg(rad) {
  return rad * 180 / Math.PI
}

/*
linear interpolation between a and b by factor t
a: starting number
b: ending number
t: interpolation factor (usually 0-1)
@number: lerped value
*/
function lerp(a, b, t){
  return a + (b - a) * t; 
}

function singleSmooth (newValue, oldValue, smoothness) {
	return (newValue * smoothness) + (oldValue * (1.0 - smoothness));
}

var smoothingBuffer = {};
function accessSmooth (newValue, smoothness, key) {
	let oldValue = newValue;
	if (smoothingBuffer.hasOwnProperty(key)) {
		oldValue = smoothingBuffer[key];
	}
	else if (gameArea.debug) {
		console.log("initializing new smoothing key");
	}
	smoothingBuffer[key] = singleSmooth(newValue, oldValue, smoothness);

	return smoothingBuffer[key];
}
